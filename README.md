# Precise Timer

This DLL adds several functions to precisely time TorqueScript operations and can measure in milliseconds or microseconds. This is useful for determining what kind of code runs faster.

Example 1:

```cs
ptimer_start("timer name");
for(%i = 0; %i < 100000; %i++)
	doSomething();
ptimer_end("timer name");
%ms_time = ptimer_duration("timer name"); //milliseconds
%us_time = ptimer_duration_usec("timer name"); //microseconds
echo("This operation took "@ %ms_time @"ms to run");
echo("This operation took "@ %us_time @"us to run");
```

Example 2:

```cs
ptimer_start("timer2");
for(%i = 0; %i < 100000; %i++)
{
	if(%i % 1000 == 0 && %i != 0)
		echo("Timer progress at "@ %i @" is: "@ ptimer_cur_duration("timer2") @"ms");

	doSomething();
}
ptimer_end("timer2");
%ms_time = ptimer_duration("timer2");
echo("This operation took "@ %ms_time @"ms to run");
```

Function list:
```
ptimer_start(name) - start the precise timer
ptimer_end(name) - end the precise timer
ptimer_cur_duration(name) - return duration of current timer in milliseconds
ptimer_cur_duration_usec(name) - return duration of current timer in microseconds
ptimer_duration(name) - return duration of ended timer in milliseconds
ptimer_duration_usec(name) - return duration of ended timer in microseconds
```

Usage notes:
 - The timers are stateless and have no overhead when running.
 - Timer names are optional. A timer started without a name is the same as an empty string.
 - Every timer created with a new name increases memory usage by a tiny bit. Try to reuse timer names.
 - Any operation longer than 2,147,483,647 microseconds (2147 seconds) will overflow if measured in microseconds.
 - The same limit applies to measuring in milliseconds, but this limit is at over 24.86 days (same time when the engine's sim time issues begin)

