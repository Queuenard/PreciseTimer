#include <chrono>
#include <iostream>
#include <map>

#include "TSFuncs.hpp"

struct timer_struct
{
	bool started = false;
	bool finished = false;
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point end;
};

std::map<std::string, timer_struct*> timer_map;

void TS_startPreciseTimer(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	delete timer_map[id];
	timer_map.erase(id);

	auto now = std::chrono::high_resolution_clock::now();
	timer_map[id] = new timer_struct { true, false, now, now };
}

void TS_endPreciseTimer(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	if(timer_map[id] == NULL)
		return;

	timer_map[id]->end = std::chrono::high_resolution_clock::now();
}

int TS_getTimerDiff_msec(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	if(timer_map[id] == NULL)
		return -1;

	return std::chrono::duration_cast<std::chrono::milliseconds>(timer_map[id]->end-timer_map[id]->start).count();
}

int TS_getTimerDiff_usec(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	if(timer_map[id] == NULL)
		return -1;

	return std::chrono::duration_cast<std::chrono::microseconds>(timer_map[id]->end-timer_map[id]->start).count();
}

int TS_getCurrentTimerDiff_msec(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	if(timer_map[id] == NULL)
		return -1;

	auto now = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(now-timer_map[id]->start).count();
}

int TS_getCurrentTimerDiff_usec(ADDR obj, int argc, const char *argv[])
{
	std::string id = "";

	if(argc >= 2 && argv[1] != NULL)
		id = std::string(argv[1]);

	if(timer_map[id] == NULL)
		return -1;

	auto now = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(now-timer_map[id]->start).count();
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	tsf_AddConsoleFunc(NULL, NULL, "ptimer_start", TS_startPreciseTimer, "(name) - start the precise timer", 1, 2);
	tsf_AddConsoleFunc(NULL, NULL, "ptimer_end", TS_endPreciseTimer, "(name) - end the precise timer", 1, 2);
	//tsf_AddConsoleFunc(NULL, NULL, "ptimer_delete", TS_endPreciseTimer, "(name) - delete the precise timer's data", 1, 2);
	tsf_AddConsoleFunc(NULL, NULL, "ptimer_cur_duration", TS_getCurrentTimerDiff_msec, "(name) - return duration of current timer in milliseconds", 1, 2);
	tsf_AddConsoleFunc(NULL, NULL, "ptimer_cur_duration_usec", TS_getCurrentTimerDiff_usec, "(name) - return duration of current timer in microseconds", 1, 2);
	tsf_AddConsoleFunc(NULL, NULL, "ptimer_duration", TS_getTimerDiff_msec, "(name) - return duration of ended timer in milliseconds", 1, 2);
	tsf_AddConsoleFunc(NULL, NULL, "ptimer_duration_usec", TS_getTimerDiff_usec, "(name) - return duration of ended timer in microseconds", 1, 2);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit(bool terminating)
{
	printf("%s: deinit'd\n", PROJECT_NAME);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, int reason, void *reserved)
{
	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit(reserved != NULL);
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
